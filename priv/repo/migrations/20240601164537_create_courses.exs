defmodule Educative.Repo.Migrations.CreateCourses do
  use Ecto.Migration

  def change do
    create table(:courses) do
      add :title, :string
      add :description, :text
      add :author, :string
      add :free, :boolean, default: false, null: false

      timestamps()

      create unique_index(:courses, [:title])
    end
  end
end
