Educative.Repo.insert!(%Educative.ELearning.Course{
  title: "Django: Python Web Development Unleashed",
  description: "Django is a free and open-source web application framework written in Python. It is used for rapid web development and clean, pragmatic design. It is built by experienced developers to make repetitive tasks easier, so you can focus on writing apps instead of reinventing the wheel. This course teaches Django for beginning and intermediate level learners. The course includes a hands-on learning experience with the help of interactive widgets. At the end of the course, you will have created a project in Django that can be used in your portfolio.",
  author: "Educative",
  free: true
})

Educative.Repo.insert!(%Educative.ELearning.Course{
  title: "Advanced React Patterns With Hooks",
  description: "With the release of hooks, certain React patterns have gone out of style. Hooks are likely the way we'll be writing React components for the next couple of years, so they are quite important. React patterns can still be used, but in most cases, developers will be better off using hooks. For example, they should choose hooks over render props or higher-order components. There may be specific cases where the aforementioned could still be used, but most of the time, hooks are better. That being said, this course will teach you some of the more advanced React patterns implemented with hooks.",
  author: "Ohans Emmanuel",
  free: false
})

Educative.Repo.insert!(%Educative.ELearning.Course{
  title: "Web Development: Unraveling HTML, CSS, and JavaScript",
  description: "HTML, CSS, and JavaScript: The trinity of web development technologies. If you're looking to get ramped up on front-end development, this course is for you. You'll find it particularly useful if you're coming from some other area of software development, although prior experience is not required. In this course, you will start with a short tour of HTML, CSS, and JS. You'll get to experiment with them through examples that explain their role and behavior. From there, you will then dive deep into each technology, exploring concepts like forms and controls in HTML, OOP in JavaScript, and the most important CSS patterns to make your web pages look good. By the end of this course, you will be able to make your own web applications that are responsive, stylized, and performant.",
  author: "Istvan Novak",
  free: false
})